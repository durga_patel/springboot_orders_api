package com.javatechie.jpa.controller;

import com.javatechie.jpa.dto.OrderRequest;
import com.javatechie.jpa.dto.OrderResponse;
import com.javatechie.jpa.entity.Customer;
import com.javatechie.jpa.entity.Product;
import com.javatechie.jpa.repository.CustomerRepository;
import com.javatechie.jpa.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1")
public class OrderController {
    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private ProductRepository productRepository;

   @PostMapping("/orders")
    public Customer placeOrder(@RequestBody OrderRequest request){
       return customerRepository.save(request.getCustomer());
    }
   
  /* @PostMapping("/product")
   public Product placeProduct(@RequestBody ProductRequest request){
      return productRepository.save(request.getProduct());
   }*/
   
   @PostMapping("/orders/{id}")
   public Customer placeOrderById(@RequestBody OrderRequest request){
      return customerRepository.save(request.getCustomer());
   }

   
   @GetMapping("/orders")
   public List<Customer> findAllOrders(){
       return customerRepository.findAll();
   }
   
   @GetMapping("/products")
   public List<Product> findAllProduct(){
       return productRepository.findAll();
   }

   
   
    @GetMapping("/orders/{id}")
    public Optional<Customer> getOrderById (@PathVariable int id){
       return customerRepository.findById(id);
    }
    
    @GetMapping("/products/{id}")
    public Optional<Product> getProductById (@PathVariable int id){
       return productRepository.findById(id);
    }
    
    @DeleteMapping("/orders/{id}")
    public String deleteOrderById(@PathVariable int id) {
    	customerRepository.deleteById(id);
    	return "deleted";
    }
    
    @DeleteMapping("/products/{id}")
    public String deleteProductById(@PathVariable int id) {
    	productRepository.deleteById(id);
    	return "deleted";
    }
    
    @GetMapping("/getInfo")
    public List<OrderResponse> getJoinInformation(){
        return customerRepository.getJoinInformation();
    }
    
    @GetMapping("/getTotalOrderAmt")
    public List<OrderResponse> getOrderAmount(){
        return customerRepository.getOrderAmount();
    }
    
}
