package com.javatechie.jpa.dto;

import org.springframework.beans.factory.annotation.Autowired;

import com.javatechie.jpa.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class OrderRequest {
	@Autowired
	private Customer customer;
}
